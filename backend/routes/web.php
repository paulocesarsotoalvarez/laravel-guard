<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/complete-registration', 'Auth\RegisterController@completeRegistration');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/2fa', function () {
    return redirect(URL()->previous());
})->name('2fa')->middleware('2fa');

// Reauthentication by the User
Route::get('/re-authenticate', 'HomeController@reauthenticate');
