<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>2FA - WebMago</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center">
                            2FActor WebMago on EC2 (AWS)
                        </h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8 offset-4">
                        <h3>Requirements</h3>
                        <ul class="text-left">
                            <li>Email account</li>
                            <li>An strong password</li>
                            <li>Google Authenticator</li>
                            <ul>
                                <li><a href="https://itunes.apple.com/us/app/google-authenticator/id388497605" target="_blank">iOS</a></li>
                                <li><a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en_us" target="_blank">Android</a></li>
                            </ul>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h3>How to Test</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="md-6">
                        <ul class="text-left">
                            <li>Register</li>
                                <ol>
                                    <li>Step 1</li>
                                        <ul>
                                            <li>Enter a Username</li>
                                            <li>Enter your Email Address</li>
                                            <li>Set an STRONG Password</li>
                                            <li>Repeat the STRONG Password</li>
                                        </ul>

                                    <li>Step 2</li>
                                        <ul>
                                            <li>A QR Barcode appears, scan using your App Google Authenticator</li>
                                            <li>Press the button <button role="button" class="btn btn-primary">Complete Registration</button> below the QR Barcode</li>
                                        </ul>
                                    <li>Step 3</li>
                                        <ul>
                                            <li>Enter the code generated on your App Google Authenticator</li>
                                        </ul>

                                </ol>
                        </ul>
                    </div>
                    <div class="md-6">
                        <ul class="text-left">
                            <li>Login</li>
                                <ol>
                                    <li>Step 1</li>
                                    <ul>
                                        <li>Enter a Username</li>
                                        <li>Enter your STRONG Password</li>
                                    </ul>
                                    <li>Step 2</li>
                                        <ul>
                                                <li>Enter the code generated on your App Google Authenticator</li>
                                        </ul>
                                </ol>
                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <p class="lead text-center">
                            {{ $_SERVER['HTTP_HOST'] }} - {{ $_SERVER['SERVER_ADDR'] }}
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </body>
</html>
