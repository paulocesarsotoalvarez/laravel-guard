# **Laravel 2FA with Google Authenticator**
  
A project Laravel Demo from Scratch to Setup 2FA to be supported on Docker/Kubernetes.  
  
**Author:**  Paulo Cesar Soto Alvarez.  
**Date:** November 2nd 2018.   

As initial part of this Zero project, we need to start creating the enviroment previously to use Docker.

1. Check if you have composer installed.  
  ```sh
  $ composer -v  
  ```
2. If the project requires to upgrade composer execute.
  ```sh
  $ composer selfupdate  
  ```
3. Download laravel installer.  
  ```sh
  $ composer global require "laravel/installer"
  ```
4. Creating project
  ```sh
  $ composer create-project --prefer-dist laravel/laravel backend 
  ``` 
5. Test if all is working.
  ```sh
  $ cd backend
  $ php artisan serve
  ```  
6. Open web browser `http://127.0.0.1:8000`  
7. Creating docker file with nginx, php directories `docker`.  
8. Creating Dockerfile.dev  `Dockerfile.dev`
9. Setting up Docker Compose `docker-compose.yml`.  
10. Creating a mysql data directory, this directory will not be tracked on git operations `mysql-db`.  
11. First run, this can took a lot of time :) go for a coffee cup meanwhile this is executed. 
    ```sh
    $ docker-compose up --build
    ```
12. Test in web browser `http://127.0.0.1:8083` the port `8083` was defined in the `docker-compose.yml`
  
## **Developing Time**
  
As we link the `laravel directory container` with the `local directory`, we can access it through the docker bash terminal to execute some commands or developing as live.

All this can reduce some time between builds process.

You can validate this on the `docker-compose.yml` lines `20`, `21`.  

- Find the laravel_zero container (*using another terminal window*), the marked in red is the id container on our enviroment tests.  
  ```sh  
  docker ps  
  ```  
     
| CONTAINER ID | IMAGE | COMMAND | CREATED | STATUS | PORTS | NAMES |  
| ------------ | ----- | ------- | ------- | ------ | ----- | ----- |  
| bfb5c5843c32 | nginx:latest | "nginx -g 'daemon of…" | 45 minutes ago | Up About a minute | 0.0.0.0:8083->80/tcp | laravel-zero_nginx_1 |  
| `0991a08add91` | laravel-zero_backend | "docker-php-entrypoi…" | 45 minutes ago | Up About a minute | 9000/tcp | laravel-zero_backend_1 |  
| d15aed4bb9aa | mysql:5.7 | "docker-entrypoint.s…" | 45 minutes ago | Up About a minute | 33060/tcp, 0.0.0.0:33065->3306/tcp | laravel-zero_database_1 |  
  
    
If you require to login into the container.  
```sh  
docker exec -it 0991a08add91 bash  
    
root@0991a08add91:/backend#  
```  
  
## **Google 2FA Setup**  
As the container has all the required libraries, we need to login it to install the source libraries.

Instructions on this Scotch.io page:  
https://scotch.io/tutorials/how-to-add-googles-two-factor-authentication-to-laravel
  
  
## **Reauthentication**  
- Using the terminal on local directory or inside the Docker Container run this:

  It will prompt for the user's email and then ask for confirmation.


  ```sh
  php artisan 2fa:reauthenticate
  ```

  - We can also provide the email using the flag --email.  
  ```sh
  php artisan 2fa:reauthenticate --email johndoe@example.com
  ```

  - To skip confirmation check, we can force the command using the --force option.  
  ```sh
  php artisan 2fa:reauthenticate --force
  ```

---  
## **Troubleshooting**  
  
 - Sometimes is required to use the database access, for this demo we had forwarding the port 3306 in the localhost to the port 3306 inside the docker container.   
   
 - Error trying to clear cache  
   ```sh 
   mkdir -p storage/framework/cache/data  
  ``` 