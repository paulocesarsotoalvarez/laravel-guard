# **Laravel 2FA with Google Authenticator**
  
Installation from scratch using the Container.  

**Author:**  Paulo Cesar Soto Alvarez.  
**Date:** November 2nd 2018.   
  
- Requirements
  - [Docker] 18.06.1-ce and up 
  - [Docker Compose] 1.22.0 and up  
  - 8 GB RAM in host  
  - 615 MB space in host
  - [Google Authenticator]

## **First Run Setup Docker**  
  
1. Execute Docker compose.  
   ```sh  
   docker-compose up  
   ```  
  
2. Run Migrations inside Docker container, first lookup which is the container ID.  
   ```sh  
   docker ps  
   ```  
    
  - Result  
  
  | CONTAINER ID | IMAGE | COMMAND | CREATED | STATUS | PORTS | NAMES |  
  | ------------ | ----- | ------- | ------- | ------ | ----- | ----- |  
  | 7749a5709428 | nginx:latest | "nginx -g 'daemon of…" | 45 minutes ago | Up About a minute | 0.0.0.0:8083->80/tcp | laravel-guard_nginx_1 |  
  | `82e2e3203c22` | demo-backend2fa:dev | "docker-php-entrypoi…" | 45 minutes ago | Up About a minute | 9000/tcp | laravel-guard_backend_1 |  
  | aa9d5c5dd1a3 | mysql:5.7 | "docker-entrypoint.s…" | 45 minutes ago | Up About a minute | 33060/tcp, 0.0.0.0:33065->3306/tcp | laravel-guard_database_1 | 
  
  - Access cotainer.  
  ```sh  
  docker exec -it  82e2e3203c22 bash  
  
  # If the command run as success you can view the bash prompt like the example below:  
  root@82e2e3203c22:/backend#  
  ```  
  
  - Run Migrations.  
  ```sh  
  php artisan migrate  
  
  # Result  
  Migration table created successfully.  
  Migrating: 2014_10_12_000000_create_users_table  
  Migrated:  2014_10_12_000000_create_users_table  
  Migrating: 2014_10_12_100000_create_password_resets_table  
  Migrated:  2014_10_12_100000_create_password_resets_table  
  Migrating: 2018_11_02_152242_add_google2fa_column_to_users  
  Migrated:  2018_11_02_152242_add_google2fa_column_to_users  
  ```  
  
3. Test It, Launch Web Browser and register a new account.  
  ```sh  
  http://127.0.0.1:8000  
  ```  
  
  If you view the QR code and add it to your GA, a number is auto-generated, this number you need to enter to complete the registration.  


[Google Authenticator]: <https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en_us>  
[Docker]: <https://docker.com>  
[Docker Compose]: <https://docker.com>  